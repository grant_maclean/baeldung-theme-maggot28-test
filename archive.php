<?php get_header(); ?>

  <div class="page-header archive-header">


  <?php if ( is_category() ) : ?>
	<?php display_archive_parent_category(); ?>
  <?php endif; ?>
  
  <?php if ( is_tag() ) : ?>
	<?php display_all_categories_of_tag(); ?>
  <?php endif; ?>
	
    <div class="flex-wrapper">

      <?php if (function_exists('ba_category_icon_template')) : ?>
        <?php ba_category_icon_template(); ?>
      <?php endif; ?>
	
      <h1 class="page-title" id="archive-title">
        <?php if (is_category()) { ?>
          <?php _e("", "wpbootstrap"); ?> <?php single_cat_title(); ?>
			  <?php if( !empty(get_query_var('tag')) ){
				  $filter_tag = get_term_by( 'slug' , get_query_var('tag'), 'post_tag');
				  echo "<span class='tag-filter-title'> » " . $filter_tag->name . "</span>";
			  } ?>
        <?php } elseif (is_tag()) { ?>
          <?php _e("Tag:", "wpbootstrap"); ?> <?php single_tag_title(); ?>
        <?php } elseif (is_author()) { ?>
          <?php _e("Posts By:", "wpbootstrap"); ?> <?php get_the_author_meta('display_name'); ?>
        <?php } elseif (is_day()) { ?>
          <?php _e("Daily Archives:", "wpbootstrap"); ?> <?php the_time('l, F j, Y'); ?>
        <?php } elseif (is_month()) { ?>
          <?php _e("Monthly Archives:", "wpbootstrap"); ?> <?php the_time('F Y'); ?>
        <?php } elseif (is_year()) { ?>
          <?php _e("Yearly Archives:", "wpbootstrap"); ?> <?php the_time('Y'); ?>
        <?php } ?>
      </h1>

    </div>

    <?php if (is_category() && !empty(category_description())) : ?>
      <div class="archive-description">
        <?php echo category_description();  ?>
      </div>
    <?php endif; ?>

    <?php if ( is_category() ) : ?>
	  <?php display_all_tags_in_category(); ?>
    <?php endif; ?>
	

	
  </div><!-- .page-header -->

  <?php if (have_posts()) :  ?>

  <div class="archive-columns layout-stacked click-whole no-more rounded-on">

    <?php while (have_posts()) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

      <div class="post-inner">

        <h3 class="post-title">
          <a href="<?php echo root_relative_url(get_the_permalink()); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
            <span class="hover-wrapper"><?php if(is_tag()||is_category()){echo ">> ";}?><?php the_title(); ?></span>
          </a>
        </h3>
		<?php if( !is_category() && !is_tag() ){?>
        <section class="post-content">
          <?php the_excerpt(); ?>
        </section> <!-- end post-content -->
		<?php } ?>
      </div> <!-- end post-inner -->

    </article> <!-- end article -->

    <?php endwhile; ?>

  </div>

  <?php if (function_exists('wp_bootstrap_page_navi')) { // if expirimental feature is active ?>

    <?php wp_bootstrap_page_navi(); // use the page navi function ?>

  <?php } else { // if it is disabled, display regular wp prev & next links ?>
    <nav class="wp-prev-next">
      <ul class="pager">
        <li class="previous"><?php next_posts_link(_e('&laquo; Older Entries', "wpbootstrap")) ?></li>
        <li class="next"><?php previous_posts_link(_e('Newer Entries &raquo;', "wpbootstrap")) ?></li>
      </ul>
    </nav>
  <?php } ?>


  <?php else : ?>

  <article id="post-not-found">
      <header>
        <h1><?php _e("No Posts Yet", "wpbootstrap"); ?></h1>
      </header>
      <section class="post_content">
        <p><?php _e("Sorry, What you were looking for is not here.", "wpbootstrap"); ?></p>
      </section>
      <footer>
      </footer>
  </article>

  <?php endif; ?>

</div> <!-- end #main -->

<?php get_sidebar(); // sidebar 1 ?>

<?php get_footer(); ?>
