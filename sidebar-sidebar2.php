<?php if ( is_active_sidebar( 'sidebar2' ) ) : ?>

  <div id="sidebar2" class="sidebar flex-col" role="complementary">

    <div class="sticky-widgets">
      <?php dynamic_sidebar( 'sidebar2' ); ?>
    </div>

  </div>

<?php endif; ?>
