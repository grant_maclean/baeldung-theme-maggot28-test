Baeldung WordPress theme
========================

Baeldung is a custom Wordpress theme.

-------------------------------------

## Theme deploy and build

### Install Grunt and Bower

Building the theme requires [node.js](http://nodejs.org/download/). We recommend you update to the latest version of npm: 
`npm install -g npm@latest`.

From the command line:

1. Install [grunt](http://gruntjs.com) and [Bower](http://bower.io/) globally with `npm install -g grunt bower`
2. Navigate to the theme directory, then run `npm install`
3. Run `bower install`

You now have all the necessary dependencies to run the build process.

### Available grunt commands

* `grunt` — Compile and optimize the files in /library directory (default)
* `grunt dev` — Compile assets when file changes are made
* `grunt` build — Just an alias for `grunt`

-------------------------------------

## Thrive Leads

Here you can read details of a theme integration with Thrive Leads plugin.

### Opt-in forms

You have ability to add custom fields to the Thrive opt-in forms. To do that, after opening the form in Thrive Content Builder, select current Lead Generation row and push "Connect with Service" button. In the opened popup window select "Edit HTML form code". Go forward and add new fields to the editor's html(ex: `<input type="hidden" name="fields[custom123]" value="success" />`). After completing the changes push "Generate Fields" button. Then push "Save" button. Changes are saved now. You can see new fields available on front-end.

-------------------------------------

## Baeldung Menus

**Due to current implementation of nav menu, if there are no any menus associated with `main_nav` menu location, the main logo wouldn't be displayed.**

### Additional fields

Additional fields are used for menu: title, description and class. Description is displayed  only for dropdown items.

### Menu

Baeldung menus has implemented custom menu icons feature coming with custom menu Walker.

#### Setup

* Go to Dashboard->Appearance->Menus click "Screen Options"(at the top-right of screen) and check "Title Attribute", "CSS Classes" and "Description" boxes.
* Select menu to edit, fill and click "Save Menu" button.

#### Menu icons

You can easily use .svg icons in the main navigation menu. You can use native Wordpress uploader.To add icon click "Set custom image" link below "Navigation Label" and "Title Attribute". To remove icon click "Remove this image" below icon image.

#### Custom menu Walker

You can find custom menu walker here `/library/yamm-nav-walker.php`.

#### Top-level menu item highlight

You can highlight top-level menu item with a checkbox called "Highlight" on menu edit screen. It is available only for top-level menu items.

#### JS Output

If menu item has an svg icon, animating scripts will be injected in footer. Each menu item will generate separate animating function. So it's dynamic stuff.

## HTML in menu item description (since 1.4.1)
You can use any HTML in menu item description field.
Description is hidden by default. To turn this on go to Dashboard -> Appearance -> Menus, open Screen Options at the top and check the Description box. Then you will see the description field in a menu item metabox.

## Optimization plugins

Be aware when using any asset optimization plugins and always add jQuery to ignore list or exclude from combining into solid file to prevent inline jQuery calls to throw js errors and break UI. For example when using with some plugins or themes which put some inline js after shortcode evaluation in content.

## Autoptimize

### Intro

Autoptimize makes optimizing your site really easy. It concatenates all scripts and styles, minifies and compresses them, adds expires headers, caches them, and moves styles to the page head and can move scripts to the footer. It also minifies the HTML code itself, making your page really lightweight. There are advanced options and an extensive API available to enable you to tailor Autoptimize to each and every site’s specific needs.

### Turning on CSS optimization (with Autoptimize):

* go to Dashboard -> Settings -> Autoptimize
* go to the CSS Options metabox
* check "Optimize CSS Code?"
* **Uncheck all other options:** - including "Also aggregate inline CSS?"

After click "Save Changes and Empty Cache".

_If you are confused where the rest of options gone just click "Show advanced settings" button at the top of Autoptimize Settings page._


### How to generate "Critical CSS"

There are a lot of tools for generation critical CSS for static pages. Below two services available online:

1. [Generator on sitelocity.com](https://www.sitelocity.com/critical-path-css-generator).
  Usage is very simple and self-explanatory. Put URL, click button, get minified CSS. Also there is lazy loaded javascript included in results but it doesn't match our needs because we use Autoptimize with lazy load CSS function.
  [Frontpage build for 1.3.6](https://drive.google.com/file/d/0B-4hrN-0MZ3ec1pZcUpLMHprMkk/view?usp=sharing)

2. [Generator on jonassebastianohlsson.com](https://jonassebastianohlsson.com/criticalpathcssgenerator/).
  This one is a bit more complicated because we need to provide full CSS from web page.
  [Frontpage build for 1.3.6](https://drive.google.com/file/d/0B-4hrN-0MZ3eZTFrdU9YUzJDQ2s/view?usp=sharing)


### Turning on JS optimization

* go to Dashboard -> Settings -> Autoptimize
* go to the JS Options metabox
* check "Optimize JavaScript Code?"
* fill "Exclude scripts from Autoptimize" with 'jquery.js, wp-includes/js, thrive-visual-editor/editor/js, thrive-headline-optimizer/frontend/js, thrive-leads/js/frontend.min.js'
* **Uncheck all other options:** - including "Also aggregate inline JS?"


## Follow Category feature (since 1.3.6)
Categories can now be followed with the help of Thrive Boxes. 
To configure this functionality - you can edit the category and set the "Thrive Box shortcode" field. 
Add the shortcode of the Thrive Box. 
For example: `[thrive_2step id='19258']`

To be clear, Thrive Box shortcodes could be found in: 
Thrive Dashboard -> Thrive Leads (/wp-admin/admin.php?page=thrive_leads_dashboard#dashboard)
