<?php
/*
Template Name: Fullwidth
Template Post Type: post
*/
?>

<?php get_header(); ?>

  <?php get_template_part('content', 'single'); ?>

</div> <!-- end #main -->

<?php get_footer(); ?>
