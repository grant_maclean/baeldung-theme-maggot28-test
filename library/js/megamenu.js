jQuery(document).ready(function($) {

    "use strict";

    // Creates a variable that gets the theme url from php
    var templateUrl = getTemplateUrl.templateUrl;

    // Adds menu-mobile class (for mobile toggle menu) before the normal menu
    // Mobile menu is hidden if width is more then 943px, but normal menu is displayed
    // Normal menu is hidden if width is below 943px, and jquery adds mobile menu
    // Done this way so it can be used with wordpress without any trouble
    // Also, we have a mobile version of the Baeldung logo

    // If width is more than 943px dropdowns are displayed on hover
    $("#menu-main-menu").each(function(){
      var timerID,
          activeID = -1,
          closeTimeout = 300,
          menu = $(this),
          hasDD = '.nav--menu_item',
          items = menu.children(hasDD),
          dropdownShow = function(id) {
            items.eq(id).find(".nav--menu_item_withDropdown").addClass("nav--menu_item_anchor_active");
            items.eq(id).find(".nav--dropdown").fadeIn(20);
            activeID = id;
          },
          dropdownHide = function(id) {
            timerID = false;
            items.eq(id).find(".nav--menu_item_withDropdown").removeClass("nav--menu_item_anchor_active");
            items.eq(id).find(".nav--dropdown").fadeOut(20);
            activeID = -1;
          };

      items.hover(function (e) {
        if (window.outerWidth > 943) {
          e.preventDefault();

          if (timerID) {
            clearTimeout(timerID);
          }

          if ($(this).index(hasDD) !== activeID) {
            if (activeID !== -1) {
              dropdownHide(activeID);
            }

            dropdownShow($(this).index(hasDD));
          }

        }
      }, function(e) {
        if (window.outerWidth > 943) {
          e.preventDefault();

          timerID = setTimeout(function() {
            dropdownHide(activeID);
          }, closeTimeout);
        }
      });
    });


    // If width is less or equal to 943px dropdowns are displayed on click
    $(".nav--menu_item_withDropdown").click(function () {
      if (window.outerWidth <= 943) {
        var dropdown = 'nav--dropdown',
            $menu = $(this).next('.' + dropdown),
            active = 'nav--menu_item_anchor_active';

        if ($menu.is(':visible')) {
          $menu.hide();
          $(this).removeClass(active);
        } else {
          $('.' + active).removeClass(active).next('.' + dropdown).hide();
          $menu.show();
          $(this).addClass(active);
        }
      }
    });

    $(".menu-mobile").click(function () {
      if (window.outerWidth <= 943) {
        $(this).toggleClass("menu-mobile-open");
      }
    });


    // The menu resets on window resize
    $(window).resize(function() {
      if (window.outerWidth > 943) {
        $(".nav--menu_item").children(".nav--dropdown").hide();
        $(".nav--menu").removeClass('show-on-mobile');
        $('.nav--menu_item_withDropdown').removeClass("nav--menu_item_anchor_active");
      }
    });

    // When clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story
    $(".menu-mobile").click(function (e) {
        $(".nav--menu").toggleClass('show-on-mobile');
        e.preventDefault();
    });

});
