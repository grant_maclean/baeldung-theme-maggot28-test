<?php

/*
 * Shortcodes
 */

//use this shortcode to display the price including country-based VAT - [price_with_vat price="XXX"]
function bd_show_vat($atts){

  $atts = shortcode_atts(
    array(
      'price' => '0',
	  'class' => '',
    ), $atts, 'price_with_vat' );

  if($atts['price']!='0'){
    $output = '<span class="price-with-vat '.$atts['class'].'" data-price="' . $atts['price'] . '"></span>';
  }

  return $output;
}

add_shortcode('price_with_vat', 'bd_show_vat');


/*
 * Sponsored tag shortcode
 */
function bd_sponsored_tag( $atts ) {
  $defaults = array ();
  $args = wp_parse_args( $atts, $defaults );

  // prepare css
  array_walk($args, function(&$a, $b) { $a = "$b: $a"; });

  // return the shortcode
  return sprintf('<span class="sponsored-tag" style="%s">%s</span>',
    implode('; ', $args),
    __('Sponsored', 'wpbootstrap'));
}

add_shortcode('sponsored', 'bd_sponsored_tag');

?>