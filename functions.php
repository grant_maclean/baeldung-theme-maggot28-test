<?php

// Yamm nav walker
require_once('library/yamm-nav-walker.php');

// Custom_Walker_Nav_Menu_Edit
require_once('library/walker-nav-menu-edit.php');

// Shortcodes
require_once('library/shortcodes.php');

// Admin Functions (commented out by default)
// require_once('library/admin.php');         // custom admin functions

// Add Translation Option
load_theme_textdomain( 'wpbootstrap', TEMPLATEPATH.'/languages' );
$locale = get_locale();
$locale_file = TEMPLATEPATH . "/languages/$locale.php";
if ( is_readable( $locale_file ) ) require_once( $locale_file );

// Clean up the WordPress Head
if( !function_exists( "wp_bootstrap_head_cleanup" ) ) {
  function wp_bootstrap_head_cleanup() {
    // remove header links
    remove_action( 'wp_head', 'feed_links_extra', 3 );                    // Category Feeds
    remove_action( 'wp_head', 'feed_links', 2 );                          // Post and Comment Feeds
    remove_action( 'wp_head', 'rsd_link' );                               // EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' );                       // Windows Live Writer
    remove_action( 'wp_head', 'index_rel_link' );                         // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );            // previous link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );             // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); // Links for Adjacent Posts
    remove_action( 'wp_head', 'wp_generator' );                           // WP version
  }
}
// Launch operation cleanup
add_action( 'init', 'wp_bootstrap_head_cleanup' );

// remove WP version from RSS
if( !function_exists( "wp_bootstrap_rss_version" ) ) {
  function wp_bootstrap_rss_version() { return ''; }
}
add_filter( 'the_generator', 'wp_bootstrap_rss_version' );

// Remove the [...] in a Read More link
if( !function_exists( "wp_bootstrap_excerpt_more" ) ) {
  function wp_bootstrap_excerpt_more( $more ) {
    global $post;
    return '...  <a href="'. get_permalink($post->ID) . '" class="more-link" title="Read '.get_the_title($post->ID).'">Read more &raquo;</a>';
  }
}
//add_filter('excerpt_more', 'wp_bootstrap_excerpt_more');

// Add WP 3+ Functions & Theme Support
if( !function_exists( "wp_bootstrap_theme_support" ) ) {
  function wp_bootstrap_theme_support() {
    add_theme_support( 'automatic-feed-links' ); // rss
    add_theme_support( 'custom-background' );    // wp custom background
    add_theme_support( 'menus' );                // wp menus
    add_theme_support( 'post-thumbnails' );      // wp thumbnails (sizes handled in functions.php)

    set_post_thumbnail_size( 125, 125, true );   // default thumb size

    register_nav_menus(                          // wp3+ menus
      array(
        'main_nav' => 'The Main Menu'   // main nav in header
      )
    );
  }
}
// launching this stuff after theme setup
add_action( 'after_setup_theme','wp_bootstrap_theme_support' );

function wp_bootstrap_main_nav() {
  // Display the WordPress menu if available
  if(has_nav_menu('main_nav')){
    wp_nav_menu(
      array(
        'menu' => 'main_nav', /* menu name */
        'theme_location' => 'main_nav',
        'menu_class' => 'nav--menu',
        'walker' => new Yamm_Nav_Walker()
      )
    );
  }
}


// Set content width
if ( ! isset( $content_width ) ) $content_width = 580;

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'wpbs-featured', 780, 300, true );
// Category icon size
add_image_size( 'category_icon', 100 );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function wp_bootstrap_register_sidebars() {
  register_sidebar(array(
    'id' => 'sidebar1',
    'name' => 'Main Sidebar',
    'description' => 'Used on every page BUT the homepage page template.',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'sidebar2',
    'name' => 'Homepage Sidebar',
    'description' => 'Used only on the homepage page template.',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'footer_columns',
    'name' => 'Footer Columns',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  register_sidebar(array(
    'id' => 'footer_stacked_rows',
    'name' => 'Footer Stacked Rows',
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));
  
  register_sidebar(array(
    'id' => 'before_post_content',
    'name' => 'Before Post Content',
    'before_widget' => '<div class="before-post-content-widget">',
    'after_widget' => '</div>',
    'before_title' => '<span style="display:none;">',
    'after_title' => '</span>',
  ));
  
  register_sidebar(array(
    'id' => 'after_post_content',
    'name' => 'After Post Content',
    'before_widget' => '<div class="after-post-content-widget">',
    'after_widget' => '</div>',
    'before_title' => '<span style="display:none;">',
    'after_title' => '</span>',
  ));


  /*
  to add more sidebars or widgetized areas, just copy
  and edit the above sidebar code. In order to call
  your new sidebar just use the following code:

  Just change the name to whatever your new
  sidebar's id is, for example:

  To call the sidebar in your template, you can just copy
  the sidebar.php file and rename it to your sidebar's name.
  So using the above example, it would be:
  sidebar-sidebar2.php

  */
} // don't remove this bracket!
add_action( 'widgets_init', 'wp_bootstrap_register_sidebars' );

/************* COMMENT LAYOUT *********************/

// Comment Layout
function wp_bootstrap_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
  <li <?php comment_class(); ?>>
    <article id="comment-<?php comment_ID(); ?>" class="clearfix">
      <div class="comment-author vcard flex-wrap">
        <div class="avatar flex-col">
          <?php echo get_avatar( $comment, $size='75' ); ?>
        </div>
        <div class="comment-text flex-col">
          <?php printf('<h4>%s</h4>', get_comment_author_link()) ?>
          <?php edit_comment_link(__('Edit','wpbootstrap'),'<span class="edit-comment btn btn-sm btn-info"><i class="fa fa-edit"></i>','</span>') ?>

                    <?php if ($comment->comment_approved == '0') : ?>
                <div class="alert alert-success">
                  <p><?php _e('Your comment is awaiting moderation.','wpbootstrap') ?></p>
                  </div>
          <?php endif; ?>

                    <?php comment_text() ?>

                    <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time('F jS, Y'); ?> </a></time>

          <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
      </div>
    </article>
    <!-- </li> is added by wordpress automatically -->
<?php
} // don't remove this bracket!

// Display trackbacks/pings callback function
function list_pings($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment;
?>
        <li id="comment-<?php comment_ID(); ?>"><i class="icon icon-share-alt"></i>&nbsp;<?php comment_author_link(); ?>
<?php

}

/************* SEARCH FORM LAYOUT *****************/

/****************** password protected post form *****/

add_filter( 'the_password_form', 'wp_bootstrap_custom_password_form' );

function wp_bootstrap_custom_password_form() {
  global $post;
  $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
  $o = '<div class="clearfix"><form class="protected-post-form" action="' . get_option('siteurl') . '/wp-login.php?action=postpass" method="post">
  ' . '<p>' . __( "This post is password protected. To view it please enter your password below:" ,'wpbootstrap') . '</p>' . '
  <label for="' . $label . '">' . __( "Password:" ,'wpbootstrap') . ' </label><div class="input-append"><input name="post_password" id="' . $label . '" type="password" size="20" /><input type="submit" name="Submit" class="btn btn-primary" value="' . esc_attr__( "Submit",'wpbootstrap' ) . '" /></div>
  </form></div>
  ';
  return $o;
}

/*********** update standard wp tag cloud widget so it looks better ************/

add_filter( 'widget_tag_cloud_args', 'wp_bootstrap_my_widget_tag_cloud_args' );

function wp_bootstrap_my_widget_tag_cloud_args( $args ) {
  $args['number'] = 20; // show less tags
  $args['largest'] = 9.75; // make largest and smallest the same - i don't like the varying font-size look
  $args['smallest'] = 9.75;
  $args['unit'] = 'px';
  return $args;
}

// filter tag clould output so that it can be styled by CSS
function wp_bootstrap_add_tag_class( $taglinks ) {
    $tags = explode('</a>', $taglinks);
    $regex = "#(.*tag-link[-])(.*)(' title.*)#e";

    foreach( $tags as $tag ) {
      $tagn[] = preg_replace($regex, "('$1$2 label tag-'.get_tag($2)->slug.'$3')", $tag );
    }

    $taglinks = implode('</a>', $tagn);

    return $taglinks;
}

add_action( 'wp_tag_cloud', 'wp_bootstrap_add_tag_class' );

add_filter( 'wp_tag_cloud','wp_bootstrap_wp_tag_cloud_filter', 10, 2) ;

function wp_bootstrap_wp_tag_cloud_filter( $return, $args )
{
  return '<div id="tag-cloud">' . $return . '</div>';
}

// Enable shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

// Disable jump in 'read more' link
function wp_bootstrap_remove_more_jump_link( $link ) {
  $offset = strpos($link, '#more-');
  if ( $offset ) {
    $end = strpos( $link, '"',$offset );
  }
  if ( $end ) {
    $link = substr_replace( $link, '', $offset, $end-$offset );
  }
  return $link;
}
add_filter( 'the_content_more_link', 'wp_bootstrap_remove_more_jump_link' );

// Remove height/width attributes on images so they can be responsive
add_filter( 'post_thumbnail_html', 'wp_bootstrap_remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'wp_bootstrap_remove_thumbnail_dimensions', 10 );

function wp_bootstrap_remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

// Add the Meta Box to the homepage template
function wp_bootstrap_add_homepage_meta_box() {
  global $post;

  // Only add homepage meta box if template being used is the homepage template
  // $post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : "");
  $post_id = $post->ID;
  $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);

  if ( $template_file == 'page-homepage.php' ){
      add_meta_box(
          'homepage_meta_box', // $id
          'Optional Homepage Tagline', // $title
          'wp_bootstrap_show_homepage_meta_box', // $callback
          'page', // $page
          'normal', // $context
          'high'); // $priority
    }
}

add_action( 'add_meta_boxes', 'wp_bootstrap_add_homepage_meta_box' );

// Field Array
$prefix = 'custom_';
$custom_meta_fields = array(
    array(
        'label'=> 'Homepage tagline area',
        'desc'  => 'Displayed underneath page title. Only used on homepage template. HTML can be used.',
        'id'    => $prefix.'tagline',
        'type'  => 'textarea'
    )
);

// The Homepage Meta Box Callback
function wp_bootstrap_show_homepage_meta_box() {
  global $custom_meta_fields, $post;

  // Use nonce for verification
  wp_nonce_field( basename( __FILE__ ), 'wpbs_nonce' );

  // Begin the field table and loop
  echo '<table class="form-table">';

  foreach ( $custom_meta_fields as $field ) {
      // get value of this field if it exists for this post
      $meta = get_post_meta($post->ID, $field['id'], true);
      // begin a table row with
      echo '<tr>
              <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
              <td>';
              switch($field['type']) {
                  // text
                  case 'text':
                      echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="60" />
                          <br /><span class="description">'.$field['desc'].'</span>';
                  break;

                  // textarea
                  case 'textarea':
                      echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="80" rows="4">'.$meta.'</textarea>
                          <br /><span class="description">'.$field['desc'].'</span>';
                  break;
              } //end switch
      echo '</td></tr>';
  } // end foreach
  echo '</table>'; // end table
}

// Save the Data
function wp_bootstrap_save_homepage_meta( $post_id ) {

    global $custom_meta_fields;

    // verify nonce
    if ( !isset( $_POST['wpbs_nonce'] ) || !wp_verify_nonce($_POST['wpbs_nonce'], basename(__FILE__)) )
        return $post_id;

    // check autosave
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;

    // check permissions
    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) )
            return $post_id;
        } elseif ( !current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
    }

    // loop through fields and save the data
    foreach ( $custom_meta_fields as $field ) {
        $old = get_post_meta( $post_id, $field['id'], true );
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta( $post_id, $field['id'], $new );
        } elseif ( '' == $new && $old ) {
            delete_post_meta( $post_id, $field['id'], $old );
        }
    } // end foreach
}
add_action( 'save_post', 'wp_bootstrap_save_homepage_meta' );

// Add thumbnail class to thumbnail links
function wp_bootstrap_add_class_attachment_link( $html ) {
    $postid = get_the_ID();
    $html = str_replace( '<a','<a class="thumbnail"',$html );
    return $html;
}
add_filter( 'wp_get_attachment_link', 'wp_bootstrap_add_class_attachment_link', 10, 1 );


// Register & enqueue script that can be used in js to generate the theme url
wp_register_script( 'getTemplateUrlScript', '' );
wp_enqueue_script( 'getTemplateUrlScript' );
$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
wp_localize_script( 'getTemplateUrlScript', 'getTemplateUrl', $translation_array );


// enqueue styles
if( !function_exists("wp_bootstrap_theme_styles") ) {
    function wp_bootstrap_theme_styles() {
        // For child themes
        wp_register_style( 'wpbs-style', get_stylesheet_directory_uri() . '/style.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'wpbs-style' );
		
        // source code pro web font for WPSyntaxHighlighter
        if (is_singular()) {
          wp_enqueue_style( 'source-code-pro', 'https://fonts.googleapis.com/css?family=Source+Code+Pro:300' );
        }

    }
}
add_action( 'wp_enqueue_scripts', 'wp_bootstrap_theme_styles' );


// enqueue javascript
if( !function_exists( "wp_bootstrap_theme_js" ) ) {
  function wp_bootstrap_theme_js(){

    if ( !is_admin() ){
      if ( is_singular() AND comments_open() AND ( get_option( 'thread_comments' ) == 1) )
        wp_enqueue_script( 'comment-reply' );
    }

    // This is the full Bootstrap js distribution file. If you only use a few components that require the js files consider loading them individually instead

    wp_register_script( 'wpbs-js', get_template_directory_uri() . '/library/dist/js/scripts.2fd9052a.min.js', array('jquery'), '1.2', true);
    wp_register_script( 'megamenu', get_template_directory_uri() . '/library/dist/js/megamenu.min.js', array('jquery'), '1.1', true);
    wp_register_script( 'vivus', get_template_directory_uri() . '/bower_components/vivus/dist/vivus.min.js', array('jquery'), '0.3.1', false);
	
    wp_add_inline_script( 'vivus', '
      function svginit_veggieBurger() {
        var svgTarget = document.getElementById(\'veggieBurger\');
        if (svgTarget) {
          veggieBurger  = new Vivus(\'veggieBurger\', {type: \'delayed\', duration: 30,  start: \'autostart\'});
        }
      }
      document.addEventListener("DOMContentLoaded", svginit_veggieBurger);
    ');


    wp_enqueue_script( 'wpbs-js' );

  }
}

add_action( 'wp_enqueue_scripts', 'wp_bootstrap_theme_js', 10 );

function bd_load_popup_assets(){
	if( is_page() && !is_front_page() ){
		wp_register_script( 'featherlight-js', get_template_directory_uri() . '/library/dist/js/featherlight.js', array('jquery'), '1.7.1', false);
		wp_enqueue_script('featherlight-js');
		wp_register_style( 'featherlight', get_stylesheet_directory_uri() . '/library/dist/css/featherlight.css', array(), '1.0', 'all' );
		wp_enqueue_style( 'featherlight' );
	}
}

add_action( 'wp_enqueue_scripts', 'bd_load_popup_assets' );

if( !function_exists( "wp_bootstrap_theme_js_deferred" ) ) {
  function wp_bootstrap_theme_js_deferred(){
    wp_enqueue_script( 'vivus' );
    wp_enqueue_script( 'megamenu' );
  }
}

add_action( 'wp_footer', 'wp_bootstrap_theme_js_deferred' );



// gives error jQuery - undefined (commented 02.06.2016)
// Load JavaScript Asynchronously
//add_filter( 'script_loader_tag', function ( $tag, $handle ) {
//    if( is_admin() ) {
//        return $tag;
//    }
//    return str_replace( ' src', ' async src', $tag );
//}, 10, 2 );


// Get <head> <title> to behave like other themes
function wp_bootstrap_wp_title( $title, $sep ) {
  global $paged, $page;

  if ( is_feed() ) {
    return $title;
  }

  // Add the site name.
  $title .= get_bloginfo( 'name' );

  // Add the site description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) ) {
    $title = "$title $sep $site_description";
  }

  // Add a page number if necessary.
  if ( $paged >= 2 || $page >= 2 ) {
    $title = "$title $sep " . sprintf( __( 'Page %s', 'wpbootstrap' ), max( $paged, $page ) );
  }

  return $title;
}
add_filter( 'wp_title', 'wp_bootstrap_wp_title', 10, 2 );

// Related Posts Function (call using wp_bootstrap_related_posts(); )
function wp_bootstrap_related_posts() {
  echo '<ul id="bones-related-posts">';
  global $post;
  $tags = wp_get_post_tags($post->ID);
  if($tags) {
    foreach($tags as $tag) { $tag_arr .= $tag->slug . ','; }
        $args = array(
          'tag' => $tag_arr,
          'numberposts' => 5, /* you can change this to show more */
          'post__not_in' => array($post->ID)
      );
        $related_posts = get_posts($args);
        if($related_posts) {
          foreach ($related_posts as $post) : setup_postdata($post); ?>
              <li class="related_post"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
          <?php endforeach; }
      else { ?>
            <li class="no_related_post">No Related Posts Yet!</li>
    <?php }
  }
  wp_reset_query();
  echo '</ul>';
}

// Numeric Page Navi (built into the theme by default)
function wp_bootstrap_page_navi($before = '', $after = '') {
  global $wpdb, $wp_query;
  $request = $wp_query->request;
  $posts_per_page = intval(get_query_var('posts_per_page'));
  $paged = intval(get_query_var('paged'));
  $numposts = $wp_query->found_posts;
  $max_page = $wp_query->max_num_pages;
  if ( $numposts <= $posts_per_page ) { return; }
  if(empty($paged) || $paged == 0) {
    $paged = 1;
  }
  $pages_to_show = 7;
  $pages_to_show_minus_1 = $pages_to_show-1;
  $half_page_start = floor($pages_to_show_minus_1/2);
  $half_page_end = ceil($pages_to_show_minus_1/2);
  $start_page = $paged - $half_page_start;
  if($start_page <= 0) {
    $start_page = 1;
  }
  $end_page = $paged + $half_page_end;
  if(($end_page - $start_page) != $pages_to_show_minus_1) {
    $end_page = $start_page + $pages_to_show_minus_1;
  }
  if($end_page > $max_page) {
    $start_page = $max_page - $pages_to_show_minus_1;
    $end_page = $max_page;
  }
  if($start_page <= 0) {
    $start_page = 1;
  }

  echo $before.'<ul class="pagination">'."";
  if ($paged > 1) {
    $first_page_text = "&laquo";
    echo '<li class="prev"><a href="'.get_pagenum_link().'" title="' . __('First','wpbootstrap') . '">'.$first_page_text.'</a></li>';
  }

  $prevposts = get_previous_posts_link( __('&larr; Previous','wpbootstrap') );
  if($prevposts) { echo '<li>' . $prevposts  . '</li>'; }
  else { echo '<li class="disabled"><a href="#">' . __('&larr; Previous','wpbootstrap') . '</a></li>'; }

  for($i = $start_page; $i  <= $end_page; $i++) {
    if($i == $paged) {
      echo '<li class="active"><a href="#">'.$i.'</a></li>';
    } else {
      echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
    }
  }
  echo '<li class="">';
  next_posts_link( __('Next &rarr;','wpbootstrap') );
  echo '</li>';
  if ($end_page < $max_page) {
    $last_page_text = "&raquo;";
    echo '<li class="next"><a href="'.get_pagenum_link($max_page).'" title="' . __('Last','wpbootstrap') . '">'.$last_page_text.'</a></li>';
  }
  echo '</ul>'.$after."";
}

// Remove <p> tags from around images
function wp_bootstrap_filter_ptags_on_images( $content ){
  return preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
}
add_filter( 'the_content', 'wp_bootstrap_filter_ptags_on_images' );


// Removes all the shortcodes from the content in RSS Feed.

function feed_remove_all_shortcodes( $content )
{
    if( is_feed() )
    {
        $content = strip_shortcodes( $content );
    }
    return $content;
}
add_filter( 'the_content', 'feed_remove_all_shortcodes' );

// Featured image in RSS feed

function sage_rss_feed_add_image() {
  global $post;
  if(has_post_thumbnail($post->ID)):
    $thumbnail = get_attachment_link(get_post_thumbnail_id($post->ID));
    echo("<image>{$thumbnail}</image>");
  endif;
}

add_action('rss2_item', __NAMESPACE__ . '\\sage_rss_feed_add_image');


/**
 * Utility function to check if a gravatar exists for a given email or id
 * @param int|string|object $id_or_email A user ID,  email address, or comment object
 * @return bool if the gravatar exists or not
 */

function validate_gravatar($id_or_email) {
  //id or email code borrowed from wp-includes/pluggable.php
  $email = '';
  if ( is_numeric($id_or_email) ) {
    $id = (int) $id_or_email;
    $user = get_userdata($id);
    if ( $user )
      $email = $user->user_email;
  } elseif ( is_object($id_or_email) ) {
    // No avatar for pingbacks or trackbacks
    $allowed_comment_types = apply_filters( 'get_avatar_comment_types', array( 'comment' ) );
    if ( ! empty( $id_or_email->comment_type ) && ! in_array( $id_or_email->comment_type, (array) $allowed_comment_types ) )
      return false;

    if ( !empty($id_or_email->user_id) ) {
      $id = (int) $id_or_email->user_id;
      $user = get_userdata($id);
      if ( $user)
        $email = $user->user_email;
    } elseif ( !empty($id_or_email->comment_author_email) ) {
      $email = $id_or_email->comment_author_email;
    }
  } else {
    $email = $id_or_email;
  }

  $hashkey = md5(strtolower(trim($email)));
  $uri = 'http://www.gravatar.com/avatar/' . $hashkey . '?d=404';

  $data = wp_cache_get($hashkey);
  if (false === $data) {
    $response = wp_remote_head($uri);
    if( is_wp_error($response) ) {
      $data = 'not200';
    } else {
      $data = $response['response']['code'];
    }
      wp_cache_set($hashkey, $data, $group = '', $expire = 60*5);

  }
  if ($data == '200'){
    return true;
  } else {
    return false;
  }
}


/*
 * Github Contact method
 */
function new_contactmethods( $contactmethods ) {
  $contactmethods['github'] = __('Github');

  return $contactmethods;
}

add_filter('user_contactmethods', 'new_contactmethods', 10, 1);


/**
 * Function to add field description on user profile edit screen
 */
function user_admin_load_scripts($hook) {
  $pages = array('profile.php', 'user-edit.php');

  if ( !in_array($hook, $pages) ) {
      return;
  }

  $js_data = '
    <script type="text/javascript">
      // Add contact method field description
      (function ($) {

        $(function() {
          var fields = [\'github\', \'googleplus\', \'twitter\'];

          $(fields).each(function(){
            var $f = $(\'#\' + this);
            if (this == \'twitter\') {
              $f.after(\'<p class="description">This will appear on your public author profile if filled in (please use the full URL)</p>\');
            } else {
              $f.after(\'<p class="description">This will appear on your public author profile if filled in</p>\');
            }

          });

        });

      })(jQuery);
    </script>
  ';

  $css_data = '
    #wpfooter {display:none;}
  ';

  wp_add_inline_script('jquery-migrate', $js_data);
  wp_add_inline_style('dashboard', $css_data);
}

add_action( 'admin_enqueue_scripts', 'user_admin_load_scripts' );


/*
 *Add logo as first menu item
 */
function add_logo_to_nav( $items, $args ) {
  if ($args->theme_location === 'main_nav') {
    $logo = '<li class="nav--logo"><a href="' . esc_url( home_url( '/', 'relative' ) ) . '" title="Baeldung"><img src="' . get_bloginfo('template_directory') . '/icon/logo.svg"></a>';
    $items = $logo . $items;
  }

  return $items;
}

add_filter( 'wp_nav_menu_items', 'add_logo_to_nav', 10, 2 );


/*
 * Whether to include custom editor styles
 */
//add_editor_style('editor-style.css');


/**
 * Add a property to a menu item
 *
 * @param object $item The menu item object.
 */
function custom_nav_menu_item( $item ) {
  $item->highlight = get_post_meta( $item->ID, '_menu_item_highlight', true );
  $item->icon = get_post_meta( $item->ID, '_menu_item_icon', true );
  $item->svgid = get_post_meta( $item->ID, '_menu_item_svgid', true );
  return $item;
}
add_filter( 'wp_setup_nav_menu_item', 'custom_nav_menu_item' );


/**
 * Save menu item custom fields' values
 * 
 * @link https://codex.wordpress.org/Function_Reference/sanitize_html_class
 */
function custom_update_nav_menu_item( $menu_id, $menu_item_db_id, $menu_item_args ){
  if ( is_array( $_POST['menu-item-highlight'] ) ) {
    $menu_item_args['menu-item-highlight'] = $_POST['menu-item-highlight'][$menu_item_db_id];
    update_post_meta( $menu_item_db_id, '_menu_item_highlight', sanitize_html_class( $menu_item_args['menu-item-highlight'] ) );
  } else {
    update_post_meta( $menu_item_db_id, '_menu_item_highlight', false );
  }

  if ( is_array( $_POST['menu-item-icon'] ) ) {
    $menu_item_args['menu-item-icon'] = $_POST['menu-item-icon'][$menu_item_db_id];
    update_post_meta( $menu_item_db_id, '_menu_item_icon', sanitize_html_class( $menu_item_args['menu-item-icon'] ) );
  } else {
    update_post_meta( $menu_item_db_id, '_menu_item_icon', false );
  }

  if ( is_array( $_POST['menu-item-svgid'] ) ) {
    $menu_item_args['menu-item-svgid'] = $_POST['menu-item-svgid'][$menu_item_db_id];
    update_post_meta( $menu_item_db_id, '_menu_item_svgid', sanitize_html_class( $menu_item_args['menu-item-svgid'] ) );
  } else {
    update_post_meta( $menu_item_db_id, '_menu_item_svgid', false );
  }
}
add_action( 'wp_update_nav_menu_item', 'custom_update_nav_menu_item', 10, 3 );


/*
 * Custom menu walker
 */
add_filter( 'wp_edit_nav_menu_walker', function( $class ){ return 'Custom_Walker_Nav_Menu_Edit'; } );


/**
 * Allow upload SVG
 */

function mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

add_filter('upload_mimes', 'mime_types', 10, 1);


/*
 * Temporary solution to bypass svg upload restriction
 */
function ignore_upload_ext($checked, $file, $filename, $mimes){

  //we only need to worry if WP failed the first pass
  if(!$checked['type']){
    //rebuild the type info
    $wp_filetype = wp_check_filetype( $filename, $mimes );
    $ext = $wp_filetype['ext'];
    $type = $wp_filetype['type'];
    $proper_filename = $filename;

    //preserve failure for non-svg images
    if($type && 0 === strpos($type, 'image/') && $ext !== 'svg'){
      $ext = $type = false;
    }

    //everything else gets an OK, so e.g. we've disabled the error-prone finfo-related checks WP just went through. whether or not the upload will be allowed depends on the <code>upload_mimes</code>, etc.

    $checked = compact('ext','type','proper_filename');
  }

  return $checked;
}

add_filter('wp_check_filetype_and_ext', 'ignore_upload_ext', 10, 4);


/* Further reading meta boxes*/
add_action( 'add_meta_boxes', 'ba_further_reading_meta' );
function ba_further_reading_meta() {
    add_meta_box( 'ba_meta_1', 'Further Reading Box', 'ba_further_reading_id_1_meta', 'post', 'normal', 'high' );
}

function ba_further_reading_id_1_meta( $post ) {
    $further_reading_id_1 = get_post_meta( $post->ID, '_further_reading_id_1', true);
    $further_reading_id_2 = get_post_meta( $post->ID, '_further_reading_id_2', true);
    $further_reading_id_3 = get_post_meta( $post->ID, '_further_reading_id_3', true);
    $further_reading_before = get_post_meta( $post->ID, '_further_reading_before', true);
    $further_reading_after = get_post_meta( $post->ID, '_further_reading_after', true);
    ?>
    <p>
        <label>Insert IDs for Further Reading articles below:</label>
    </p>
    <?php if (!empty($further_reading_id_1) && (!('publish' == get_post_status($further_reading_id_1)))) { echo '<p style="color: red">The post doesn\'t exist or not published:</p>';} ?>
    <p>
        <input type="text" name="further_reading_id_1" value="<?php echo esc_attr( $further_reading_id_1 ); ?>" />
    </p>
    <?php if (!empty($further_reading_id_2) && (!('publish' == get_post_status($further_reading_id_2)))) { echo '<p style="color: red">The post doesn\'t exist or not published:</p>';} ?>
    <p>
        <input type="text" name="further_reading_id_2" value="<?php echo esc_attr( $further_reading_id_2 ); ?>" />
    </p>
    <?php if (!empty($further_reading_id_3) && (!('publish' == get_post_status($further_reading_id_3)))) { echo '<p style="color: red">The post doesn\'t exist or not published:</p>';} ?>
    <p>
        <input type="text" name="further_reading_id_3" value="<?php echo esc_attr( $further_reading_id_3 ); ?>" />
    </p>
    <p>
        <label for="further_reading_before">Show Further Reading before post content <input type="checkbox" name="further_reading_before" value="1" <?php if (!empty(esc_attr( $further_reading_before))) { echo "checked"; }; ?> /></label>
    </p>
    <p>
        <label for="further_reading_after">Show Further Reading after post content <input type="checkbox" name="further_reading_after"value="1" <?php if (!empty(esc_attr( $further_reading_after))) { echo "checked"; }; ?> /></label>
    </p>
    <p>
        Use [fr-box] shortcode to display the box anywhere within a post.
    </p>

    <?php
}

add_action( 'save_post', 'ba_further_reading_meta_save_meta' );
function ba_further_reading_meta_save_meta( $post_ID ) {
    global $post;
    if( $post->post_type == "post" ) {
        if (isset( $_POST ) ) {
            update_post_meta( $post_ID, '_further_reading_id_1', strip_tags( $_POST['further_reading_id_1'] ) );
            update_post_meta( $post_ID, '_further_reading_id_2', strip_tags( $_POST['further_reading_id_2'] ) );
            update_post_meta( $post_ID, '_further_reading_id_3', strip_tags( $_POST['further_reading_id_3'] ) );
            update_post_meta( $post_ID, '_further_reading_before', strip_tags( $_POST['further_reading_before'] ) );
            update_post_meta( $post_ID, '_further_reading_after', strip_tags( $_POST['further_reading_after'] ) );
        }
    }
}


function ba_further_reading_hooked($content) {
    global $post;
    $custom_content = '';
    if (get_post_meta( $post->ID, '_further_reading_before', true)== '1') {
        $custom_content = ba_further_reading_markup();
    }
    $custom_content .= $content;
    if (get_post_meta( $post->ID, '_further_reading_after', true)== '1') {
        $custom_content = $custom_content.ba_further_reading_markup();;
    }

    return $custom_content;
}
add_filter( 'the_content', 'ba_further_reading_hooked' );



function ba_further_reading_markup() {
	
    global $post;
	
	if ( is_single() ) :
	
		$related = array();
		$id_1 = get_post_meta( $post->ID, '_further_reading_id_1', true);
		$id_2 = get_post_meta( $post->ID, '_further_reading_id_2', true);
		$id_3 = get_post_meta( $post->ID, '_further_reading_id_3', true);

		if (((!empty($id_1)) && ('publish' == get_post_status($id_1))) || ((!empty($id_2)) && ('publish' == get_post_status($id_2))) || ((!empty($id_3)) && ('publish' == get_post_status($id_3)))) {
			if (!empty($id_1) && ('publish' == get_post_status($id_1))) { $related[] = $id_1; }
			if (!empty($id_2) && ('publish' == get_post_status($id_2))) { $related[] = $id_2; }
			if (!empty($id_3) && ('publish' == get_post_status($id_3))) { $related[] = $id_3; }

			$output = '<section class="further-reading-posts">';
			$output .= '<h1>Further reading:</h1>';
			$output .= '<div class="fr-wrap">';

			foreach ($related as $related_post) {
				$post_title = get_the_title($related_post);
				$post_excerpt = get_the_excerpt($related_post);
				$post_permalink = get_the_permalink($related_post);
				$output .= '<div class="fr-item"><h2><a href="'.$post_permalink.'">'.$post_title.'</a></h2><div class="ex">'.$post_excerpt.'</div><div class="ex"><a href="'.$post_permalink.'">Read more</a> &rarr;</div></div>';
			}

			$output .= '</div>';
			$output .= '</section>';

			return $output;
		} else {
			return false;
		}
		
	else :
	
		return false;
	
	endif;

}

add_shortcode( 'fr-box', 'ba_further_reading_markup' );



/*
 * Embeds further reading data as JSON into the footer
 */
 
function ba_further_reading_json_embed(){
	global $post;
	if ( is_single() ):
		$related = array();
		
		$id_1 = get_post_meta( $post->ID, '_further_reading_id_1', true);
		$id_2 = get_post_meta( $post->ID, '_further_reading_id_2', true);
		$id_3 = get_post_meta( $post->ID, '_further_reading_id_3', true);
		
		$meta = array();

		if (((!empty($id_1)) && ('publish' == get_post_status($id_1))) || ((!empty($id_2)) && ('publish' == get_post_status($id_2))) || ((!empty($id_3)) && ('publish' == get_post_status($id_3)))) {
			if (!empty($id_1) && ('publish' == get_post_status($id_1))) { $related[] = $id_1; }
			if (!empty($id_2) && ('publish' == get_post_status($id_2))) { $related[] = $id_2; }
			if (!empty($id_3) && ('publish' == get_post_status($id_3))) { $related[] = $id_3; }

			foreach ($related as $related_post) {
				$arr=array(
					'title'     => get_the_title($related_post),
					'excerpt'   => get_the_excerpt($related_post),
					'permalink' => get_the_permalink($related_post),
				);
				array_push($meta, $arr);
			}

		} else {
			//no further reading articles
		}
		
		echo "\n<script>\n/* <![CDATA[ */\n";
		echo "var fr_data=".json_encode( $meta ) . ";";	
		echo "\n/* ]]> */\n</script>\n";
	endif;
	return;
}

add_action( 'wp_footer', 'ba_further_reading_json_embed' );



/*
 * Embed the post terms as JSON into the footer
 */
 
 function ba_embed_post_terms_json(){
	 
	 global $post;
	
	 $cats_out = array();
	 $tags_out = array();
		 
	 if( is_single() ) :
	 
		 $cats = get_the_terms($post, 'category');
		 $tags = get_the_terms($post, 'post_tag');

		 foreach($cats as $cat) :
			array_push($cats_out, $cat->name);
			
			// add parent category
			if(  $cat->parent != 0 ){
				$category_parent = get_term( $cat->parent, 'category' );
				array_push($cats_out, $category_parent->name);
				
				// add grandparent category
				if(  $category_parent->parent != 0 ){
					$category_grandparent = get_term( $category_parent->parent, 'category' );
					array_push($cats_out, $category_grandparent->name);
				}
			}
			
		 endforeach;
		 if(!empty($tags)) :
			 foreach($tags as $tag) :
				array_push($tags_out, $tag->name);
			 endforeach;
		 endif;
	
	elseif ( is_category() ) :
	
		$thisCat = get_category(get_query_var('cat'),false);
		array_push($cats_out, $thisCat->name);

	elseif ( is_tag() ) :
	
		$thisTag = get_term_by('slug', get_query_var('tag'), 'post_tag');
		array_push($tags_out, $thisTag->name);
	
	endif;
	
	echo "\n<script> \n/* <![CDATA[ */\n";
	echo "var ba_cats=".json_encode( $cats_out ) . ";\n";
	echo "var ba_tags=".json_encode( $tags_out ) . ";";	
	echo "\n/* ]]> */\n</script>\n";
		
	return;
	
 }
 add_action( 'wp_footer', 'ba_embed_post_terms_json' );

 
 
/*
 * Share the Category with a Thrive box opt-in [feature]
 */

// Add custom Thrive Box field
function ba_category_thrive_box_add($term){
?>
  <tr class="form-field">
    <td>
      <label for="thrive_box"><?php _e( 'Thrive Box shortcode', 'sage' ); ?></label>
      <input type="text" name="thrive_box" id="thrive_box" value="">
      <p class="description">
        <?php _e( 'Enter Thrive Box shortcode to be used with subscribe popup.' ); ?>
      </p>
    </td>
  </tr>

<?php
}

add_action('category_add_form_fields', 'ba_category_thrive_box_add', 10, 2);


// Edit custom Thrive Box field
function ba_category_thrive_box_edit($term){
  $term_meta = get_term_meta( $term->term_id, 'thrive_box', true );
  $tax_value = esc_attr( $term_meta ) ? esc_attr( $term_meta ) : '';
?>

  <tr class="form-field">
    <th scope="row" valign="top">
      <label for="thrive_box">
        <?php _e( 'Thrive Box shortcode', 'sage' ); ?>
      </label>
    </th>
    <td>
      <input type="text" name="thrive_box" id="thrive_box" value="<?php echo $tax_value; ?>">
      <p class="description">
        <?php _e( 'Enter Thrive Box shortcode to be used with subscribe popup.' ); ?>
      </p>
    </td>
  </tr>

<?php
}

add_action('category_edit_form_fields', 'ba_category_thrive_box_edit', 10, 2);


// Save taxonomy page
function ba_category_thrive_box_save( $term_id ) {
 if ( isset( $_POST['thrive_box'] ) ) {
    $term_meta = $_POST['thrive_box'];
    add_term_meta( $term_id, 'thrive_box', $term_meta, true );
  }
}

add_action( 'create_category', 'ba_category_thrive_box_save', 10, 2 );


// Update taxonomy page
function ba_category_thrive_box_update ( $term_id ) {
  if ( isset( $_POST['thrive_box'] ) ) {
    $term_meta = $_POST['thrive_box'];
    update_term_meta ( $term_id, 'thrive_box', $term_meta );
  } else {
    update_term_meta ( $term_id, 'thrive_box', '' );
  }
}

add_action( 'edited_category', 'ba_category_thrive_box_update', 10, 2 );

/*
 * Whether to display the post tag a page
 */

// Add custom field field to the post tag
function ba_post_tag_field_add($term){
?>
  <tr class="form-field">
    <td>
      <label for="thrive_box"><?php _e( 'Display on a page', 'sage' ); ?></label>
      <input type="checkbox" name="post_tag_display" id="post_tag_display">
      <p class="description">
        <?php _e( 'Whether to display this tag on a page?' ); ?>
      </p>
    </td>
  </tr>

<?php
}

add_action('post_tag_add_form_fields', 'ba_post_tag_field_add', 10, 2);


// Edit post tag field
function ba_post_tag_field_edit($term){
  $term_meta = get_term_meta( $term->term_id, 'post_tag_display', true );
  $tax_value = esc_attr( $term_meta ) ? 'checked' : '';
?>

  <tr class="form-field">
    <th scope="row" valign="top">
      <label for="thrive_box">
        <?php _e( 'Display on a page', 'sage' ); ?>
      </label>
    </th>
    <td>
      <input type="checkbox" name="post_tag_display" id="post_tag_display" <?php echo $tax_value; ?>>
      <p class="description">
        <?php _e( 'Whether to display this tag on a page?' ); ?>
      </p>
    </td>
  </tr>

<?php
}

add_action('post_tag_edit_form_fields', 'ba_post_tag_field_edit', 10, 2);


// Save taxonomy page
function ba_post_tag_field_save( $term_id ) {
 if ( isset( $_POST['post_tag_display'] ) ) {
    $term_meta = $_POST['post_tag_display'];
    add_term_meta( $term_id, 'post_tag_display', $term_meta, true );
  }
}

add_action( 'create_post_tag', 'ba_post_tag_field_save', 10, 2 );


// Update taxonomy page
function ba_post_tag_field_update ( $term_id ) {
  if ( isset( $_POST['post_tag_display'] ) ) {
    $term_meta = $_POST['post_tag_display'];
    update_term_meta ( $term_id, 'post_tag_display', $term_meta );
  } else {
    update_term_meta ( $term_id, 'post_tag_display', '' );
  }
}

add_action( 'edited_post_tag', 'ba_post_tag_field_update', 10, 2 );


/*
 * Whether to display the post tag a category page
 */

// Add custom field to the post tag
function ba_post_tag_show_in_category_field_add($term){
?>
  <tr class="form-field">
    <td>
      <label for="thrive_box"><?php _e( 'Display on a category page', 'sage' ); ?></label>
      <input type="checkbox" name="post_tag_category_display" id="post_tag_category_display">
      <p class="description">
        <?php _e( 'Whether to display this tag on a page?' ); ?>
      </p>
    </td>
  </tr>

<?php
}

add_action('post_tag_add_form_fields', 'ba_post_tag_show_in_category_field_add', 10, 2);


// Edit post tag field
function ba_post_tag_show_in_category_field_edit($term){
	
	// tag's display control
	if ( metadata_exists('term',  $cat_post_tag->term_id, 'post_tag_category_display') ){
		$term_meta  = get_term_meta( $cat_post_tag->term_id, 'post_tag_category_display', true );
	}else{
		$term_meta = "on";
	}

	$tax_value = esc_attr( $term_meta ) ? 'checked' : '';
?>

  <tr class="form-field">
    <th scope="row" valign="top">
      <label for="thrive_box">
        <?php _e( 'Display on a category page', 'sage' ); ?>
      </label>
    </th>
    <td>
      <input type="checkbox" name="post_tag_category_display" id="post_tag_category_display" <?php echo $tax_value; ?>>
      <p class="description">
        <?php _e( 'Whether to display this tag on a category page?' ); ?>
      </p>
    </td>
  </tr>

<?php
}

add_action('post_tag_edit_form_fields', 'ba_post_tag_show_in_category_field_edit', 10, 2);


// Save taxonomy page
function ba_post_tag_show_in_category_field_save( $term_id ) {
 if ( isset( $_POST['post_tag_category_display'] ) ) {
    $term_meta = $_POST['post_tag_category_display'];
    add_term_meta( $term_id, 'post_tag_category_display', $term_meta, true );
  }
}

add_action( 'create_post_tag', 'ba_post_tag_show_in_category_field_save', 10, 2 );


// Update taxonomy page
function ba_post_tag_show_in_category_field_update ( $term_id ) {
  if ( isset( $_POST['post_tag_category_display'] ) ) {
    $term_meta = $_POST['post_tag_category_display'];
    update_term_meta ( $term_id, 'post_tag_category_display', $term_meta );
  } else {
    update_term_meta ( $term_id, 'post_tag_category_display', '' );
  }
}

add_action( 'edited_post_tag', 'ba_post_tag_show_in_category_field_update', 10, 2 );


/*
 * Number of articles a tag belongs to in a certain category for it to be displayed on the category page
 */


// Edit post tag field
function ba_post_tag_count_limit_field_edit($term){
	// tag's display control
	if ( metadata_exists('term',  $term->term_id, 'post_tag_count_limit') ){
		$term_meta  = get_term_meta( $term->term_id, 'post_tag_count_limit', true );
	}else{
		$term_meta = 2;
	}
?>

  <tr class="form-field">
    <th scope="row" valign="top">
      <label for="thrive_box">
        <?php _e( 'Count limit', 'sage' ); ?>
      </label>
    </th>
    <td>
      <input type="number" min="1" max="100" name="post_tag_count_limit" id="post_tag_count_limit" value="<?php echo $term_meta;?>">
      <p class="description">
        <?php _e( 'Number of articles a tag belongs to in a certain category for it to be displayed on the category page' ); ?>
      </p>
    </td>
  </tr>

<?php
}

add_action('edit_category_form_fields', 'ba_post_tag_count_limit_field_edit', 10, 2);

// save extra category extra fields callback function
function save_count_limit_field( ) {
    if ( isset( $_POST['post_tag_count_limit'] ) ) {
        update_term_meta($_POST['tag_ID'], 'post_tag_count_limit', $_POST['post_tag_count_limit']);
    }
}

add_action ( 'edited_category', 'save_count_limit_field');



/*
 * Completely hide comment messages
 */

/* Create meta boxes */
function hide_comments_add_meta_boxes() {

  add_meta_box(
    'hide_comments',
    __( 'Hide comments' ),
    'hide_comments_meta_box',
    array('post', 'page'),
    'side',
    'default'
  );

}

/* Display the meta box. */
function hide_comments_meta_box( $post ) { ?>

  <?php wp_nonce_field( basename( __FILE__ ), 'hide_comments_nonce' ); ?>

  <p>
    <label for="hide_comments-field">
      <input type="checkbox" name="hide_comments" id="hide_comments-field" value="1"  <?php checked( get_post_meta( $post->ID, 'hide_comments', true) ); ?> />
      <?php _e( "Hide comments" ); ?>
    </label>
  </p>
<?php }

/* Save the meta box's post metadata. */
function hide_comments_save_meta( $post_id, $post ) {

  /* Verify the nonce before proceeding. */
  if ( !isset( $_POST['hide_comments_nonce'] ) || !wp_verify_nonce( $_POST['hide_comments_nonce'], basename( __FILE__ ) ) )
    return $post_id;

  /* Get the post type object. */
  $post_type = get_post_type_object( $post->post_type );

  /* Check if the current user has permission to edit the post. */
  if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
    return $post_id;

  /* Get the posted data and sanitize it for use as an HTML class. */
  $new_meta_value = ( isset( $_POST['hide_comments'] ) ? sanitize_html_class( $_POST['hide_comments'] ) : '' );

  /* Get the meta key. */
  $meta_key = 'hide_comments';

  /* Get the meta value of the custom field key. */
  $meta_value = get_post_meta( $post_id, $meta_key, true );

  /* If a new meta value was added and there was no previous value, add it. */
  if ( $new_meta_value && '' == $meta_value )
    add_post_meta( $post_id, $meta_key, $new_meta_value, true );

  /* If the new meta value does not match the old value, update it. */
  elseif ( $new_meta_value && $new_meta_value != $meta_value )
    update_post_meta( $post_id, $meta_key, $new_meta_value );

  /* If there is no new meta value but an old value exists, delete it. */
  elseif ( '' == $new_meta_value && $meta_value )
    delete_post_meta( $post_id, $meta_key, $meta_value );
}

/* Meta box setup function. */
function hide_comments_meta_boxes_setup() {

  /* Add meta boxes on the 'add_meta_boxes' hook. */
  add_action( 'add_meta_boxes', 'hide_comments_add_meta_boxes' );

  /* Save post meta on the 'save_post' hook. */
  add_action( 'save_post', 'hide_comments_save_meta', 10, 2 );
}

/* Fire meta box setup function on the post editor screen. */
add_action( 'load-post.php', 'hide_comments_meta_boxes_setup' );
add_action( 'load-post-new.php', 'hide_comments_meta_boxes_setup' );


/*
 * Allow HTML in menu item description
 */

remove_filter('nav_menu_description', 'strip_tags');

function cus_wp_setup_nav_menu_item($menu_item) {
  $menu_item_description = preg_replace("/\n(?!<br>)/", "<br>", $menu_item->post_content);
  $menu_item->description = apply_filters('nav_menu_description', $menu_item_description );
  return $menu_item;
}

add_filter( 'wp_setup_nav_menu_item', 'cus_wp_setup_nav_menu_item' );


/*
 * AquaSVG/Thrive Leads state-switch bug fix
 */

remove_action( 'before_wp_tiny_mce', array( 'Aqua_SVG_Sprite', 'localize_shortcode_button_scripts' ) );


/*
 * Thrive leads disappearing state button bug fix
 */

add_action('tcb_hook_editor_head','thrive_state_button_fix');

function thrive_state_button_fix(){
  echo "
  <style>
    .states-button-container {
      display: block !important;
    }
  </style>";
}


/*
 * Add the Title Tag theme support
 */

function add_the_title_tag_theme_support() {
  add_theme_support( 'title-tag' );
}

add_action('after_setup_theme', 'add_the_title_tag_theme_support');


/*
 * Optimization Tweak: Remove the WPSyntaxHighlighter plugin assets on posts without the <pre>'formatted content.
 */

function ba_remove_syntaxhighlighted_everywhere() {
  global $post;

  $content = get_post_field('post_content', $post->ID);

  if (is_singular() && !empty($content)) {

    preg_match("/<pre/", $content, $match);

    if ( empty($match) ) {
      wp_dequeue_style('core3.0');
      wp_dequeue_style('core-Default3.0');
      wp_dequeue_style('theme-Default3.0');
    }
  }
}

add_action('wp_print_styles', 'ba_remove_syntaxhighlighted_everywhere');

/*
 * Custom archive excerpt
 */
function ba_custom_archive_excerpt( $output ) {
  global $post;

  if (is_archive() && !is_author()) {
    return sprintf('%s&nbsp;<a href="%s" title="%s" class="more">&gt;&gt;&gt;</a>',
      $output,
      get_the_permalink($post->ID),
      esc_attr($post->post_title)
    );
  }

  return $output;
}

add_filter( 'get_the_excerpt', 'ba_custom_archive_excerpt' );

/*
 * Show all tags on the current category archive page
 */
 
function display_all_tags_in_category(){

	// get all the tags of all the posts in this category and the counts
	$this_category = get_queried_object();
	$tag_filter = get_query_var('tag');
	
	//number of time a tag has been used in this categories' articles to be displayed
	$count_limit  = get_term_meta( $this_category->term_id, 'post_tag_count_limit', true );
	$count_limit = $count_limit ? $count_limit : 2;

	$related_tags = array();
	$all_posts_in_category = get_posts( array("category"=> $this_category->term_id, "posts_per_page" => -1) );

	foreach ( $all_posts_in_category as $cat_post ) :

		$cat_post_tags = wp_get_post_tags($cat_post->ID);
		foreach ($cat_post_tags as $cat_post_tag) :
			
			// tag's display control
			if ( metadata_exists('term',  $cat_post_tag->term_id, 'post_tag_category_display') ){
				$display_tag  = get_term_meta( $cat_post_tag->term_id, 'post_tag_category_display', true );
			}else{
				$display_tag = "on";
			}
					
			if ( array_key_exists( $cat_post_tag->name, $related_tags ) && ($display_tag == 'on') ){
				$related_tags[$cat_post_tag->name]['count']++;
			}else{
				$related_tags[$cat_post_tag->name] = array(
					'name'  => $cat_post_tag->name,
					'count' =>1,
					'slug'  =>$cat_post_tag->slug,
				);
			}
			
		endforeach;

	endforeach; 
	
	// sort by count 
	function ba_sort_by_tag_count($a, $b) {
	  return $b["count"] - $a["count"];
	}
	usort($related_tags, "ba_sort_by_tag_count");		
	
	// output tags
	echo '<ul class="related-tags">';
	foreach ( $related_tags as $tag ){
		if($tag['count'] < $count_limit){ break; }
		if($tag['slug']==$tag_filter){
			$tag_url = "/category/".$this_category->slug;
			$tag_label = " <span>x  </span>" . $tag['name'] ;
			$tag_class = "highlight";
		}else{
			$tag_url = "/category/". $this_category->slug . "/tag/" . $tag['slug'];
			$tag_label = $tag['name'];
			$tag_class="";
		}
		echo "<li><a class='".$tag_class."' href='" . $tag_url . "'>". $tag_label. " (" . $tag['count'] . ")</a></li>";
	}
	echo "</ul>";

}

/*
 * Show all categories on the current tag archive page
 */
 
function display_all_categories_of_tag(){

	// get all the categories of all the posts in this tag
	$this_tag = get_queried_object();

	$related_cats = array();
	
	$all_posts_in_tag = get_posts( array("tag_id"=> $this_tag->term_id, "posts_per_page" => -1) );

	foreach ( $all_posts_in_tag as $tagged_post ) :

		$tagged_post_categories = wp_get_post_categories($tagged_post->ID, array('fields'=>'all'));
		
		// add parent categories to list of categories
		$parent_cats = array();
		foreach ($tagged_post_categories as $post_cat) :
			
			if( $post_cat->parent != 0 ){
				$the_parent = get_term($post_cat->parent);
				$parent_cats[] = $the_parent;
				
				if( $the_parent->parent != 0 ){
					$the_grandparent = get_term($the_parent->parent);
					$parent_cats[] = $the_grandparent;
				}
			}	
			
		endforeach;
		
		$tagged_post_categories = array_merge( $tagged_post_categories, $parent_cats );
		
		foreach ($tagged_post_categories as $post_cat) :

			if ( array_key_exists( $post_cat->name, $related_cats ) ){
				$related_cats[$post_cat->name]['count']++;
			}else{
				$related_cats[$post_cat->name] = array(
					'name'    => $post_cat->name,
					'count'   => 1,
					'slug'    => $post_cat->slug,
					'term_id' => $post_cat->term_id,
				);
			}
			
		endforeach;

	endforeach; 	


	// sort by count 
	function ba_sort_by_cat_count($a, $b) {
	  return $b["count"] - $a["count"];
	}
	usort($related_cats, "ba_sort_by_cat_count");	
	
	// output tags
	echo '<ul class="related-categories">';
	foreach ( $related_cats as $cat ){

		$count_limit  = get_term_meta( $cat['term_id'], 'post_tag_count_limit', true );
		$count_limit = $count_limit ? $count_limit : 2;
		if( $cat['count'] < $count_limit) {continue;}
		
		$tag_url = "/category/". $cat['slug'] . "/tag/" . $this_tag->slug;
		$tag_label = $cat['name'];
		$icon = "<img class='cat-up-icon' src='" . get_stylesheet_directory_uri() . "/images/up-icon-white.png'/>";

		echo "<li><a class='archive-parent-cat' href='" . $tag_url . "'>" . $icon . $tag_label . "</a></li>";
	}
	echo "</ul>";

}


/*
 * Show category parent tag (for use in archive/category templates)
 */

function display_archive_parent_category(){
	$current_category = get_category(get_query_var('cat'),false);
	$icon = "<img class='cat-up-icon' src='" . get_stylesheet_directory_uri() . "/images/up-icon-white.png'/>";
	if ( $current_category->parent != '0' ){
		$category_parent = get_term( $current_category->parent, 'category' );
		echo "<div><a class='archive-parent-cat' href='/category/" . $category_parent->slug . "'>" . $icon .  $category_parent->name . "</a></div>";
	}
}

/*
 * Feature: setup the category icon image
 */

// Add Icon field
function ba_category_icon_add($term){
?>
  <tr class="form-field">
    <td>
      <label for="ba_category_icon">
        <?php _e( 'Front-end icon', 'sage' ); ?>
      </label>
      <?php echo ba_get_image_uploader_admin_html($term); ?>
      <p class="description">
        <?php _e( 'Select an image' ); ?>
      </p>
    </td>
  </tr>

<?php
}

add_action('category_add_form_fields', 'ba_category_icon_add', 10, 2);


// Edit custom Thrive Box field
function ba_category_icon_edit($term){
?>

  <tr class="form-field">
    <th scope="row" valign="top">
      <label for="ba_category_icon">
        <?php _e( 'Front-end icon', 'sage' ); ?>
      </label>
    </th>
    <td>
      <?php echo ba_get_image_uploader_admin_html($term); ?>
    </td>
  </tr>

<?php
}

add_action('category_edit_form_fields', 'ba_category_icon_edit', 10, 2);


// Save taxonomy page
function ba_category_icon_save( $term_id ) {
 if ( isset( $_POST['ba_category_icon'] ) ) {
    $term_meta = $_POST['ba_category_icon'];
    add_term_meta( $term_id, 'ba_category_icon', $term_meta, true );
  }
}

add_action( 'create_category', 'ba_category_icon_save', 10, 2 );


// Update taxonomy page
function ba_category_icon_update( $term_id ) {
  if ( isset( $_POST['ba_category_icon'] ) ) {
    $term_meta = $_POST['ba_category_icon'];
    update_term_meta ( $term_id, 'ba_category_icon', $term_meta );
  } else {
    update_term_meta ( $term_id, 'ba_category_icon', '' );
  }
}

add_action( 'edited_category', 'ba_category_icon_update', 10, 2 );

function ba_get_image_uploader_admin_html($term = false, $option = '') {
  global $post;

  $term_meta = get_term_meta( $term->term_id, 'ba_category_icon', true );
  ob_start();
?>
  <p class="field-icon description description-wide" id="field-icon">
      <?php if ( !empty($option) && empty($term) ): ?>
        <?php $icon = get_option($option); ?>

        <span class="category-img-container" style="display: inline-block; background-color: #eee;">
          <img id='image-preview' src='<?php echo wp_get_attachment_url( $icon ); ?>' width="100px">
        </span>

        <span class="hide-if-no-js">
          <a class="upload-custom-img <?php if ( !empty($icon) ) { echo 'hidden'; } ?>" href="<?php echo esc_url( get_upload_iframe_src( 'image', $post->ID )); ?>">
            <?php _e('Set custom image') ?>
          </a>
          <a class="delete-custom-img <?php if ( empty($icon) ) { echo 'hidden'; } ?>" href="#">
            <?php _e('Remove this image') ?>
          </a>
        </span>

        <!-- A hidden input to set and post the chosen image id -->
        <input type="hidden" type="text" id="ba_category_icon_default" class="widefat edit-category-icon" name="ba_category_icon_default" value="<?php echo esc_attr( $icon ) ?>" />

      <?php elseif( !empty($term) ): ?>

        <span class="category-img-container" style="display: inline-block; background-color: #eee;">
          <?php if ( !empty($term_meta) ) : ?>
            <img id='image-preview' src='<?php echo wp_get_attachment_url( $term_meta ); ?>' width="100px">
          <?php endif; ?>
        </span>

        <span class="hide-if-no-js">
          <a class="upload-custom-img <?php if ( !empty($term_meta) ) { echo 'hidden'; } ?>" href="<?php echo esc_url( get_upload_iframe_src( 'image', $post->ID )); ?>">
            <?php _e('Set custom image') ?>
          </a>
          <a class="delete-custom-img <?php if ( empty($term_meta) ) { echo 'hidden'; } ?>" href="#">
            <?php _e('Remove this image') ?>
          </a>
        </span>

          <!-- A hidden input to set and post the chosen image id -->
        <input type="hidden" type="text" id="ba_category_icon" class="widefat edit-category-icon" name="ba_category_icon" value="<?php echo esc_attr( $term_meta ); ?>" />

      <?php endif; ?>

      <?php if ( !empty($option) || !empty($term) ): ?>

        <?php wp_enqueue_media(); ?>

        <script type="text/javascript">
          jQuery(function($){

            // Set all variables to be used in scope
            var frame,
                metaBox = $('#field-icon'), // Your meta box id here
                addImgLink = metaBox.find('.upload-custom-img'),
                delImgLink = metaBox.find( '.delete-custom-img'),
                imgContainer = metaBox.find( '.category-img-container'),
                imgIdInput = metaBox.find( '.edit-category-icon' );

            // ADD IMAGE LINK
            addImgLink.on( 'click', function( event ){

              event.preventDefault();

              // If the media frame already exists, reopen it.
              if ( frame ) {
                frame.open();
                return;
              }

              // Create a new media frame
              frame = wp.media({
                title: 'Select or Upload Media Of Your Chosen Persuasion',
                button: {
                  text: 'Use this media'
                },
                //library: {
                    //type: 'image/svg+xml'
                //},
                multiple: false  // Set to true to allow multiple files to be selected
              });


              // When an image is selected in the media frame...
              frame.on( 'select', function() {

                // Get media attachment details from the frame state
                var attachment = frame.state().get('selection').first().toJSON();

                // Send the attachment URL to our custom image input field.
                imgContainer.append( '<img src="'+attachment.url+'" alt="" width="100px" id="image-preview"/>' );

                // Send the attachment id to our hidden input
                imgIdInput.val( attachment.id );

                // Hide the add image link
                addImgLink.addClass( 'hidden' );

                // Unhide the remove image link
                delImgLink.removeClass( 'hidden' );
              });

              // Finally, open the modal on click
              frame.open();
            });

            // DELETE IMAGE LINK
            delImgLink.on( 'click', function( event ){

              event.preventDefault();

              // Clear out the preview image
              imgContainer.html( '' );

              // Un-hide the add image link
              addImgLink.removeClass( 'hidden' );

              // Hide the delete image link
              delImgLink.addClass( 'hidden' );

              // Delete the image id from the hidden input
              imgIdInput.val( '' );

            });

          });
        </script>

      <?php endif; ?>

    </p>
<?php 
  return ob_get_clean();
}


/*
 * Register setting for a default category icon
 */

function ba_category_icon_default_callback( $val ){
  $id = $val['id'];
  echo ba_get_image_uploader_admin_html($term, $val['option_name']);
}

function add_option_field_to_general_admin_page(){
  $option_name = 'ba_category_icon_default';

  register_setting( 'reading', $option_name, 'esc_attr' );

  add_settings_field(
    'ba_category_icon_default_ID',
    __( 'Category default icon' , 'baeldung' ),
    'ba_category_icon_default_callback',
    'reading',
    'default',
    array(
      'id' => 'ba_category_icon_default_ID',
      'option_name' => $option_name
    )
  );
}

add_action('admin_menu', 'add_option_field_to_general_admin_page');


/*
 * Get category icon for archive page template
 */
function ba_category_icon_template() {

  // get the term object
  $queried_object = get_queried_object();
  // setup term id
  $term_id = $queried_object->term_id;
  // get default icon
  $icon_default = get_option('ba_category_icon_default');
  // term icon
  $term_meta = get_term_meta( $term_id, 'ba_category_icon', true );
  // select one
  $icon = !empty($term_meta) ? $term_meta : $icon_default;

  // icon full URL
  $icon_url = wp_get_attachment_url( $icon );

  // cache matches
  $icon_svg = array();
  // check for an svg
  preg_match("/(.*\/)([\w\s-_]*)\.svg$/", $icon_url, $icon_svg);

?>

<?php // Put the category icon HTML ?>
<?php echo getCategoryIconHTML($term_id); ?>

<?php if (!empty($icon_svg)) : ?>

  <script>

    function svginit_CategoryIcon() {

      // add helper animation class
      var svgClass = document.getElementById("CategoryIcon").getAttribute("class");
      document.getElementById("CategoryIcon").setAttribute("class", svgClass + " ready");

      // init an icon
      CategoryIcon = new Vivus('CategoryIcon', {type: 'delayed', duration: 60,  start: 'autostart'});

    }

    // add event listener to fire when Vivus is loaded
    document.addEventListener("ready-to-animate-svg", svginit_CategoryIcon);

  </script>

<?php endif; ?>

<?php
}


/*
 * Get category icon HTML
 */

function getCategoryIconHTML($term_id){

  // get category icon
  $term_icon_id = get_term_meta( $term_id, 'ba_category_icon', true );
  // get default category icon
  $default_icon_id = get_option('ba_category_icon_default');

  // select one
  $icon_id = !empty($term_icon_id) ? $term_icon_id : $default_icon_id;

  // return if nothing
  if ( empty($icon_id) ) return;

  // get icon sources for fullwidth size to get calculated for svg
  $icon_src = wp_get_attachment_image_src( $icon_id, 'fullwidth' );

  // cache matches
  $icon_type_svg = array();
  // check for svg
  preg_match("/(.*\/)([\w\s-_]*)\.svg$/", $icon_src[0], $icon_type_svg);

  // cache attributes
  $atts = ' class="category-icon" id="CategoryIcon" xmlns="http://www.w3.org/2000/svg" version="1.1"';

  // if svg
  if (!empty($icon_type_svg)) {
    // path
    $icon_path = get_attached_file($icon_id);
    // check/get contents
    $icon_html = file_exists($icon_path) ? file_get_contents($icon_path) : '';

    // remove width and height atts
    $icon_html = preg_replace("/<svg[^>]*(viewBox=[0-9 '\"]+)[^>]*/", "<svg $1" . $atts, $icon_html);

  // if not svg
  } else {
    // get icon sources for category_icon predefined size
    $icon_src = wp_get_attachment_image_src( $icon_id, 'category_icon' );
    // prepare html output
    $icon_html = sprintf('<img src="%s" width="%s" height="%s" alt="" %s>',
      root_relative_url($icon_src[0]), $icon_src[1], $icon_src[2], $atts
    );
  }

  // return the icon
  return sprintf('<div class="icon-wrapper">%s</div>', $icon_html);
}

// Responsive iframe video
function ba_filter_iframe_videos( $content ){
  return preg_replace_callback("/<iframe([^>]*?width=['\"](\d+)['\"][^>]*?height=['\"](\d+)['\"]|)[^>]*?><\/iframe>/", function ($matches) {
    $ratio_default = 60;
    $ratio_calc = !empty($matches[1]) ? ( round( intval($matches[3]) / intval($matches[2]), 3 ) * 100 ) : $ratio_default;
    return sprintf('<span class="iframe-fluid" style="padding-bottom: %s">%s</span>',
      $ratio_calc . '%', $matches[0]
    );
  }, $content);
}

add_filter( 'the_content', 'ba_filter_iframe_videos' );



// get an array of post ids that have the noindex ON meta value set by the All In One SEO metabox
function get_noindex_post_ids(){

	$args = array(
		"posts_per_page" => -1,
		"post_type" 	 => array('post', 'page'),
		'meta_key' 		 => '_aioseop_noindex', //AllInOne metakey
		'meta_value' 	 => 'on',
	);
	
	$noindex_posts = get_posts($args);
	
	$exclude_ids = array();
	
	foreach($noindex_posts as $post){
		$exclude_ids[] = $post->ID;
	}

	return $exclude_ids;
	
}

// pass a list of post ids that have the noindex property to the Better WordPress XML Sitemaps plugin to exclude them from sitemaps
add_filter('bwp_gxs_excluded_posts', 'bwp_gxs_exclude_posts', 10, 2);
function bwp_gxs_exclude_posts($post_ids, $post_type)
{
	return get_noindex_post_ids();
}

// Hide article index page from robots
add_action('wp_head', function(){
  if(is_home()) {
    echo '<!-- Hide article index from robots start -->
           <meta name="robots" content="noindex, follow" />
         <!-- Hide article index from robots end -->';
  }
});

// pretty permalinks for tag filtering on category pages
function ba_category_rewrite_rule() {
    add_rewrite_rule(
        '^category/([^/]*)/tag/([^/]*)/page/([0-9]{1,})/?',
        'index.php?post_type=post&category_name=$matches[1]&tag=$matches[2]&paged=$matches[3]',
        'top'
    );
    add_rewrite_rule(
        '^category/([^/]*)/tag/([^/]*)',
        'index.php?post_type=post&category_name=$matches[1]&tag=$matches[2]',
        'top'
    );

}
add_action( 'init', 'ba_category_rewrite_rule' );


/**
 * Make a URL relative
 */
function root_relative_url($input) {

  if($input){
	//Skip attachment resource array (image)
	return $input;  
  }
  
  $url = parse_url($input);

  // Skip relative URL in certain cases
  if (in_array(true, [
    is_feed(), // RSS feed
    isset($_GET['sitemap']), // Sitemaps
    in_array($GLOBALS['pagenow'], ['wp-login.php', 'wp-register.php']), // Login pages
    (false !== strpos($_SERVER['REQUEST_URI'], '.xml')), // XML sitemap pages,
    (!isset($url['host']) || !isset($url['path'])), // Explicit Relative URLs
  ])) return $input; // don't modify the URL

  $site_url = parse_url(network_home_url());  // falls back to home_url

  if (!isset($url['scheme'])) {
    $url['scheme'] = $site_url['scheme'];
  }

  $hosts_match = $site_url['host'] === $url['host'];
  $schemes_match = $site_url['scheme'] === $url['scheme'];
  $ports_exist = isset($site_url['port']) && isset($url['port']);
  $ports_match = ($ports_exist) ? $site_url['port'] === $url['port'] : true;

  if ($hosts_match && $schemes_match && $ports_match) {
    return wp_make_link_relative($input);
  }

  return $input;
}

/**
 * Hooks a single callback to multiple tags
 */
function add_filters($tags, $function, $priority = 10, $accepted_args = 1) {
  foreach ((array) $tags as $tag) {
    add_filter($tag, $function, $priority, $accepted_args);
  }
}


/**
 * Root relative URLs
 *
 * WordPress likes to use absolute URLs on everything - let's clean that up.
 *
 */

add_filters(apply_filters('bael/relative-url', [

  'bloginfo_url', // The URL returned by get_bloginfo()

  'get_pagenum_link', // Paginated link
  'get_comments_pagenum_link', // Paginated comment link
  'get_comment_link', // Link to a comment

  'term_link', // The term link
  'search_link', // The search link
  'author_link', // The URL to the author page for the user with the ID provided
  'the_author_posts_link', // The link to the author page of the author of the current post

  // Date archive links
  'day_link',
  'month_link',
  'year_link',

  'image_send_to_editor', // The image HTML markup to send to the editor when inserting an image
  'wp_get_attachment_thumb_url', // The attachment thumbnail URL
  'wp_get_attachment_image_src', // The image src result

  'the_tags', // the tags list for a given post
  'wp_list_pages', // The HTML output of the pages to list
  'wp_list_categories', // The HTML output of a taxonomy list

  'script_loader_src', // The script loader source
  'style_loader_src', // An enqueued style’s fully-qualified URL

  'rest_url', // The REST URL
  'plugins_url', // The URL to the plugins directory
  'theme_file_uri', // The URL to a file in the theme
  'parent_theme_file_uri', // The URL to a file in the parent theme
  'template_directory_uri', // The current theme directory URI
  'stylesheet_directory_uri', // Stylesheet directory
  'get_theme_root_uri', // The URI for themes directory

]), 'root_relative_url');

/* Don't forget to filter srcset for responsive images */
add_filter('wp_calculate_image_srcset', function ($sources) {
  foreach ((array) $sources as $source => $src) {
    $sources[$source]['url'] = root_relative_url($src['url']);
  }
  return $sources;
});

// Helper function to get 100% absolute URLs
function absolute_url($input) {
  $url = parse_url($input);

  $site_url = parse_url(network_home_url());  // falls back to home_url

  if ( (!isset($url['scheme']) || !isset($url['host'])) && isset($url['path']) ) {

    // prepend slash ahead relative paths without leading slash
    if (preg_match('/^[^\/].*/', $url['path'])) {
      $url['path'] = '/' . $url['path'];
    }

    return $site_url['scheme'] . '://' . $site_url['host'] . $url['path'];
  }

  return $input;
}


// Hardcode full path in URL for canonical links and shortlinks
add_filters(apply_filters('bael/absolute-path-links', [
  'get_shortlink',
  'get_canonical_url',
  'aioseop_canonical_url'
]), function ($url) {

  $url = preg_replace("/^(?:https?:\/\/|\/)?(?:(?:[a-zA-Z0-9\-]*\.)+[a-zA-Z0-9\-]*)?(\/.*)?$/i", 'https://www.baeldung.com\1', $url);

  return $url;
});


// Make Root Relative URLs compatible with OpenGraph by AIOSEOP
add_filter('aiosp_opengraph_meta', function($value, $meta, $data){


  // Grab post/image URLs
  if ( in_array($meta, ['facebook', 'twitter']) && in_array($data, ['url', 'thumbnail', 'twitter_thumbnail']) )  {
    // make absolute
    return absolute_url($value);
  }

  // fallback to default
  return $value;

}, 10, 3);


// Fix the link URLs in wpDiscuz confirmation email messages
// Possibly fix any other root relative links within wp_mail() output
add_filter('wp_mail', function($args){

  $args['message'] = preg_replace_callback('/(?<=href=\")\/.*?(?=\")/', function($matches){

    return absolute_url($matches[0]);

  }, $args['message']);

  return $args;

});


// Insert footer widget area after the post content
// note: post start widget is simply in the post template 
// this is done to get correct order with Thrive leads content. 
function bd_add_footer_widget_to_content( $content ) { 
	global $post;   
    if( is_single() ) {
		ob_start();
		echo "<div class='after-post-widgets'>";
		dynamic_sidebar('after_post_content');
		echo "</div>";
        $content .= ob_get_clean();
    }
    return $content;
}
add_filter( 'the_content', 'bd_add_footer_widget_to_content' );

// sticky widget weights
// *********************

//Add input fields(priority 5, 3 parameters)
add_action('in_widget_form', 'kk_in_widget_form',5,3);
//Callback function for options update (priorität 5, 3 parameters)
add_filter('widget_update_callback', 'kk_in_widget_form_update',5,3);
//add class names (default priority, one parameter)
add_filter('dynamic_sidebar_params', 'kk_dynamic_sidebar_params');

function kk_in_widget_form($t,$return,$instance){
    $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '') );

    if ( !isset($instance['widget_weight']) )
        $instance['widget_weight'] = '1';
    ?>
	<p>
	<label>Sticky Weight</label>
    <input type="text" name="<?php echo $t->get_field_name('widget_weight'); ?>" id="<?php echo $t->get_field_id('widget_weight'); ?>" value="<?php echo $instance['widget_weight'];?>" />
	</p>
    <?php
    $retrun = null;
    return array($t,$return,$instance);
}

function kk_in_widget_form_update($instance, $new_instance, $old_instance){

    $instance['widget_weight'] = strip_tags($new_instance['widget_weight']);
    return $instance;
}

function kk_dynamic_sidebar_params($params){
    global $wp_registered_widgets;
    $widget_id = $params[0]['widget_id'];
    $widget_obj = $wp_registered_widgets[$widget_id];
    $widget_opt = get_option($widget_obj['callback'][0]->option_name);
    $widget_num = $widget_obj['params'][0]['number'];
    if (isset($widget_opt[$widget_num]['widget_weight'])){
		$widget_weight = $widget_opt[$widget_num]['widget_weight'];
	}else{
		$widget_weight = '1';
	}
    
	$params[0]['before_widget'] = preg_replace('/class="/', ' data-sticky-weight="'.$widget_weight.'" ' . ' class="',  $params[0]['before_widget'], 1);
    
	return $params;
}

/* function to check if current post belongs to the given parent category - for use in Widget Logic */
function is_desc_cat($cats, $_post = null) {
  foreach ((array)$cats as $cat) {
    if (in_category($cat, $_post)) {
      return true;
    } else {
      if (!is_int($cat)) $cat = get_cat_ID($cat);
      $descendants = get_term_children($cat, 'category');
      if ($descendants && in_category($descendants, $_post)) return true;
    }
  }

return false;
}



/*** widget page height display condition (rest of functionailty in scripts.js) ***/ 

//Add input fields(priority 5, 3 parameters)
add_action('in_widget_form', 'ba_height_limit_in_widget_form',5,3);
//Callback function for options update (priority 5, 3 parameters)
add_filter('widget_update_callback', 'ba_height_limit_in_widget_form_update',5,3);
//add custom data parameter containing field value
add_filter('dynamic_sidebar_params', 'ba_height_limit_dynamic_sidebar_params');

function ba_height_limit_in_widget_form($t,$return,$instance){
    $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '') );

    if ( !isset($instance['widget_height_limit']) )
        $instance['widget_height_limit'] = '';
    ?>
	<p>
	<label>Hide below page height of:</label>
    <input type="number" name="<?php echo $t->get_field_name('widget_height_limit'); ?>" id="<?php echo $t->get_field_id('widget_height_limit'); ?>" value="<?php echo $instance['widget_height_limit'];?>" />px
	</p>
    <?php
    $retrun = null;
    return array($t,$return,$instance);
}

function ba_height_limit_in_widget_form_update($instance, $new_instance, $old_instance){
    $instance['widget_height_limit'] = strip_tags($new_instance['widget_height_limit']);
    return $instance;
}

function ba_height_limit_dynamic_sidebar_params($params){
    global $wp_registered_widgets;
    $widget_id = $params[0]['widget_id'];
    $widget_obj = $wp_registered_widgets[$widget_id];
    $widget_opt = get_option($widget_obj['callback'][0]->option_name);
    $widget_num = $widget_obj['params'][0]['number'];
	
    $widget_height_limit = (isset($widget_opt[$widget_num]['widget_height_limit']) ? $widget_opt[$widget_num]['widget_height_limit'] : 0);
	$params[0]['before_widget'] = preg_replace('/class="/', ' data-height-limit="'.$widget_height_limit.'" ' . ' class="',  $params[0]['before_widget'], 1);
	
	return $params;
}

/*** widget non-sticky widget option ***/ 

//Add input fields(priority 5, 3 parameters)
add_action('in_widget_form', 'ba_nonsticky_in_widget_form',5,3);
//Callback function for options update (priority 5, 3 parameters)
add_filter('widget_update_callback', 'ba_nonsticky_in_widget_form_update',5,3);
//add custom data parameter containing field value
add_filter('dynamic_sidebar_params', 'ba_nonsticky_dynamic_sidebar_params');

function ba_nonsticky_in_widget_form($t,$return,$instance){
    $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '') );

    if ( !isset($instance['widget_nonsticky']) )
        $instance['widget_nonsticky'] = '';
    ?>
	<p>
	<label>Non sticky?:</label>
    <input type="checkbox" name="<?php echo $t->get_field_name('widget_nonsticky'); ?>" id="<?php echo $t->get_field_id('widget_nonsticky'); ?>" value="1" <?php checked( $instance['widget_nonsticky'],'1', true );?> />
	</p>
    <?php
    $retrun = null;
    return array($t,$return,$instance);
}

function ba_nonsticky_in_widget_form_update($instance, $new_instance, $old_instance){
    $instance['widget_nonsticky'] = strip_tags($new_instance['widget_nonsticky']);
    return $instance;
}

function ba_nonsticky_dynamic_sidebar_params($params){
    global $wp_registered_widgets;
    $widget_id = $params[0]['widget_id'];
    $widget_obj = $wp_registered_widgets[$widget_id];
    $widget_opt = get_option($widget_obj['callback'][0]->option_name);
    $widget_num = $widget_obj['params'][0]['number'];

    $widget_nonsticky = $widget_opt[$widget_num]['widget_nonsticky'] ? 'nonsticky' : 'sticky';
	$params[0]['before_widget'] = preg_replace('/class="/', ' '.$widget_opt[$widget_num]['widget_nonsticky'].' data-stickyness="'.$widget_nonsticky.'" ' . ' class="',  $params[0]['before_widget'], 1);
	
	return $params;
}



/*** remove auto paragraphs from all widgets ***/
remove_filter('widget_text_content', 'wpautop');

/**
 * Removes the original author meta box and replaces it
 * with a customized version.
 */
add_action( 'add_meta_boxes', 'bd_replace_post_author_meta_box' );
function bd_replace_post_author_meta_box() {
    $post_type = get_post_type();
    $post_type_object = get_post_type_object( $post_type );

    if ( post_type_supports( $post_type, 'author' ) ) {
        if ( is_super_admin() || current_user_can( $post_type_object->cap->edit_others_posts ) ) {
            remove_meta_box( 'authordiv', $post_type, 'core' );
            add_meta_box( 'authordiv', __( 'Author', 'text-domain' ), 'bd_post_author_meta_box', null, 'normal' );
        }
    }
}

/**
 * Display form field with list of authors.
 * Modified version of post_author_meta_box().
 *
 * @global int $user_ID
 *
 * @param object $post
 */
function bd_post_author_meta_box( $post ) {
    global $user_ID;
?>
<label class="screen-reader-text" for="post_author_override"><?php _e( 'Author', 'text-domain' ); ?></label>
<?php
    wp_dropdown_users( array(
        'role__in' => [ 'administrator', 'author', 'contributor', 'editor', 'new2author', 'new2contributor'], // Add desired roles here.
        'name' => 'post_author_override',
        'selected' => empty( $post->ID ) ? $user_ID : $post->post_author,
        'include_selected' => true,
        'show' => 'display_name_with_login',
    ) );
}

/* remove emoji support */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );







/**
 * Add elements after each heading for use in # link scroll positioning.
 * IDs are taken from the heading or auto generated from the heading text
 * BAEL-17967 & BAEL-17957
 */
 
add_filter( 'the_content', 'bd_add_heading_anchor_points', 1);
function bd_add_heading_anchor_points($content){
	
	$used_ids = array('test');
	
	//regex loop through all headings in the post content
	$new_content = preg_replace_callback('#<h([2-3])(.*?)>(.*?)</h\d>#imsu', function ($m){
	
		global $used_ids; $useExisitingID = false; $exisitingID = "";
		
		//--assign the matches more meaningful names
		//h1, h2, h3 etc
		$headingIndex = (int)$m[1];
		$html_atts = $m[2];
		$heading_text = $m[3];
		
		
		//remove html from heading text leaving just text
		$label = trim(strip_tags($heading_text));
		
		
		//handle empty headings
		if ($label == '') {
			return $m[0];
		}

		
		//check for existing id
		$id_matches = array();
		preg_match( '/id="([^"]*)"/i', $html_atts, $id_matches );
		
		//if there is an existing id
		if(count($id_matches)){
			//use the exisiting id
			$headingId = $id_matches[1];
			//and remove it from the heading atts (because it will be used in the anchor div instead)
			$html_atts = str_replace($id_matches[0],'',$html_atts);
		}else{
			//or convert the heading text into an id format: This Is A Heading => this-is-a-heading
			$headingId = bd_generate_id_from_heading_text($label);
		}
		

		//make sure the id is unique, if not generate a new one
		$idToTry = $headingId;
		$foundUnique = false; $dup_index = 1;
		while(!$foundUnique){
			if ( !in_array($idToTry, $used_ids) ){
				$used_ids[] = $idToTry;
				$foundUnique = true;
				$headingId = $idToTry;
			}else{
				$idToTry = $headingId . '-' . $dup_index;
				$dup_index++;
			}
		}
		
		//remove <br> and other line-breaking characters tags from inside the heading element, these cause the anchor links to go onto the next line
		$heading_text = str_replace( array('<br>','<br/>','<br />'), '', $heading_text);
		$heading_text = str_replace( array("\r\n", "\n", "\r"), ' ', $heading_text);
		
		//build new heading html
		return '<h' . $headingIndex . $html_atts . ' data-id="' . $headingId . '">' . $heading_text . '</h' . $headingIndex . '>' . '<div class="bd-anchor" id="'.$headingId.'"></div>';	


	}, $content);
	return $new_content;
}

function bd_generate_id_from_heading_text($text){
	
	//make lowercase
	$text = strtolower($text);
	
	//remove number prefix common in our headings by checking if the second pos has a .
	if( substr($text,1,1)=="." ){
		$text = substr($text, 2);
	}
	
	//trim whitespace
	$text = trim($text);
	
	//replace remaining white space with hyphens
	$id = str_replace(' ', '-', $text);
	
	//remove all non-alphanumeric characters which are not supposed to be in # links
	$id = preg_replace('/[^a-zA-Z0-9-]/', '', $id);
	
	return $id;
}

add_filter('wp_mail', 'remove_brackets_mail_link', 20, 1);
function remove_brackets_mail_link($args){
  $args['message'] = preg_replace('/<(' . preg_quote(network_site_url(), '/') . '[^>]*)>/', '\1', $args['message']);
  return $args;
}