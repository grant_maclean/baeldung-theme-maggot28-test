<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
  <meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage" itemid="https://google.com/article"/>
  <meta itemprop="datePublished" content="<?php echo get_the_date(DateTime::ISO8601); ?>"/>
  <meta itemprop="dateModified" content="<?php the_modified_time(DateTime::ISO8601); ?>"/>

  <header>

    <!-- <?php the_post_thumbnail( 'wpbs-featured', array('class' => 'webfeedsFeaturedVisual') ); ?> -->

    <div class="page-header"><h1 class="single-title entry-title" itemprop="headline"><?php the_title(); ?></h1>
      <p class="post-modified"><?php _e( "Last modified:", "wpbootstrap" ); ?> <span class="updated"><?php the_modified_time( 'F j, Y' ); ?></span></p>

      <div class="meta-row">
        <div class="author vcard" itemprop="author" itemscope itemtype="https://schema.org/Person">
          <span class="author-by"> by </span>
          <span class="author-name fn" itemprop="name">
            <?php the_author_posts_link(); ?>
          </span>
        </div>
        <span itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
          <meta itemprop="url" content="<?php echo absolute_url(get_the_post_thumbnail_url( get_the_ID(), 'full' )); ?>">
          <meta itemprop="width" content="208">
            <meta itemprop="height" content="208">
        </span>
        <span itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
          <span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
              <meta itemprop="url" content="<?php echo absolute_url(get_the_post_thumbnail_url( get_the_ID(), 'full' )); ?>">
              <meta itemprop="width" content="208">
              <meta itemprop="height" content="208">
          </span>
          <meta itemprop="name" content="Baeldung">
        </span>

        <?php
        /*
         * Post Categories
         */
        ?>
        <ul class="categories">
          <?php
            // get post terms for category tax
            $terms = wp_get_post_terms( get_the_ID(), 'category' );

            foreach ($terms as $term) {
              // get Thrive Box shortcode field
              $term_meta  = get_term_meta( $term->term_id, 'thrive_box', true );

              // share category if not empty
              if (!empty($term_meta)) {
                $matches = array();
                // prepare shortcode
                $term_field = preg_replace("/^\[(\S*)(\s[^]]*?)?\](.*)?/", "[$1$2]+[/$1]", trim($term_meta));
                // run shortcode
                $term_share = do_shortcode($term_field);

                // display category tag with a share lightbox
                printf('<li><a href="%s" rel="category tag">%s</a><span class="btn-share" title="%s">%s</span></li>',
                  get_term_link($term->term_id),
                  $term->name,
                   __('Subscribe for', 'sage') . '&nbsp;' . $term->name,
                  $term_share
                );
              } else {
                // otherwise display default category
                printf('<li><a href="%s" rel="category tag">%s</a></li>',
                  get_term_link($term->term_id),
                  $term->name
                );

              }

            }
          ?>
        </ul>

        <?php
        /*
         * Post Tags
         */
        ?>
        <ul class="post-tags">
          <?php
            // get post terms
            $terms = wp_get_post_terms( get_the_ID(), 'post_tag' );

            foreach ($terms as $term) {
              // get term field
              $term_meta  = get_term_meta( $term->term_id, 'post_tag_display', true );

              if (!empty($term_meta)) {
                // display the post tag
                printf('<li><a href="%s" rel="post tag">%s</a></li>',
                  get_term_link($term->term_id),
                  $term->name
                );
              }

            }
          ?>

        </ul>
      </div>
    </div>


  </header> <!-- end article header -->
  <div class='before-post-widgets'>
  <?php dynamic_sidebar('before_post_content');?>
  </div>
  
  <section class="post-content clearfix" itemprop="articleBody">
    <?php the_content(); ?>

    <?php wp_link_pages(); ?>

  </section> <!-- end article section -->
  
  <!-- <footer>

    <?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","wpbootstrap") . ':</span> ', ' ', '</p>'); ?>

    <?php
    // only show edit button if user has permission to edit posts
    if( current_user_can('edit_posts') ) {
    ?>
    <a href="<?php echo get_edit_post_link(); ?>" class="btn btn-success edit-post"><i class="icon-pencil icon-white"></i> <?php _e("Edit post","wpbootstrap"); ?></a>
    <?php } ?>

  </footer>--> <!-- end article footer -->

</article> <!-- end article -->

  <?php if ( empty(get_post_meta( $post->ID, 'hide_comments', true )) ) : ?>
    <?php comments_template('', true); ?>
  <?php endif; ?>

  <?php if (!comments_open()) { ?>
    <span>Comments are closed on this article!</span>
  <?php } ?>

<?php endwhile; ?>

<?php else : ?>

<article id="post-not-found">
    <header>
      <h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
    </header>
    <section class="post-content">
      <p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
    </section>
    <footer>
    </footer>
</article>

<?php endif; ?>
